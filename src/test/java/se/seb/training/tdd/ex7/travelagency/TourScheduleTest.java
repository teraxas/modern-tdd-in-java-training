package se.seb.training.tdd.ex7.travelagency;

import org.joda.time.LocalDate;
import org.junit.Test;
import se.edument.domain.framework.BDDTest;

/**
 * Created by S86571 on 2017.04.27.
 */
public class TourScheduleTest extends BDDTest<TourScheduleCommandHandler, TourScheduledAggregate> {

	public static final String TEST_TOUR_NAME = "TestTour";
	public static final LocalDate TEST_TOUR_DATE = new LocalDate(2017, 4, 27);

	@Test
	public void canScheduleTour() {
		ScheduleTour command = new ScheduleTour(TEST_TOUR_NAME, TEST_TOUR_DATE, 5);
		TourScheduled tourScheduledEvent = new TourScheduled(TEST_TOUR_NAME, TEST_TOUR_DATE, 5, command.getId());
		test(given(), when(command), then(tourScheduledEvent));
	}

	@Test
	public void upToThreeToursPerDay() {
		ScheduleTour command = new ScheduleTour(TEST_TOUR_NAME, TEST_TOUR_DATE, 5);
		test(given(command, command, command),
				when(command),
				thenFailWith(TooManyToursException.class));
	}

}
