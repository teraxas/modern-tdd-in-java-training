package se.seb.training.tdd.ex6.typewriter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by S86571 on 2017.04.27.
 */
public class TypewriterTest {

	public static final String TEST_STRING = "Test string";

	private Typewriter typewriter;
	private List<String> logOutput;

	@Before
	public void before() {
		this.logOutput = new ArrayList<>();
		this.typewriter = new Typewriter();
	}

	@Test
	public void logCallInput() {
		typewriter.setLogger(s -> logOutput.add(s));

		typewriter.sendInput(TEST_STRING);

		Assert.assertEquals(1, logOutput.size());
		Assert.assertEquals(TEST_STRING, logOutput.get(0));
	}

	@Test
	public void noLogSet() {
		try {
			typewriter.sendInput(TEST_STRING);
		} catch (NullPointerException e) {
			Assert.fail();
		}
	}
}
