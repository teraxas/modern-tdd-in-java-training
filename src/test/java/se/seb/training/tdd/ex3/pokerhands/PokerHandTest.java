package se.seb.training.tdd.ex3.pokerhands;

import org.junit.Assert;
import org.junit.Test;

public class PokerHandTest {
	private static final Hand SEVEN_HIGH = new Hand(
		new Card(Card.TWO, Card.HEARTS),
		new Card(Card.THREE, Card.SPADES),
		new Card(Card.FIVE, Card.CLUBS),
		new Card(Card.SIX, Card.CLUBS),
		new Card(Card.SEVEN, Card.HEARTS)
	);
	private static final Hand QUEEN_HIGH = new Hand(
		new Card(Card.TWO, Card.HEARTS),
		new Card(Card.FOUR, Card.SPADES),
		new Card(Card.SEVEN, Card.CLUBS),
		new Card(Card.NINE, Card.CLUBS),
		new Card(Card.QUEEN, Card.HEARTS)
	);
	private static final Hand PAIR_OF_TWO = new Hand(
		new Card(Card.FOUR, Card.DIAMONDS),
		new Card(Card.TWO, Card.SPADES),
		new Card(Card.THREE, Card.CLUBS),
		new Card(Card.TWO, Card.SPADES),
		new Card(Card.JACK, Card.DIAMONDS)
	);
	private static final Hand PAIR_OF_NINE = new Hand(
		new Card(Card.FIVE, Card.SPADES),
		new Card(Card.THREE, Card.HEARTS),
		new Card(Card.NINE, Card.HEARTS),
		new Card(Card.KING, Card.CLUBS),
		new Card(Card.NINE, Card.SPADES)
	);
	private static final Hand TRIPS_OF_EIGHT = new Hand(
		new Card(Card.EIGHT, Card.CLUBS),
		new Card(Card.EIGHT, Card.HEARTS),
		new Card(Card.FIVE, Card.DIAMONDS),
		new Card(Card.JACK, Card.SPADES),
		new Card(Card.EIGHT, Card.DIAMONDS)
	);
	private static final Hand TRIPS_OF_JACK = new Hand(
		new Card(Card.JACK, Card.HEARTS),
		new Card(Card.JACK, Card.DIAMONDS),
		new Card(Card.FOUR, Card.SPADES),
		new Card(Card.EIGHT, Card.DIAMONDS),
		new Card(Card.JACK, Card.CLUBS)
	);
	private static final Hand JACK_HIGH_FLUSH = new Hand(
		new Card(Card.THREE, Card.CLUBS),
		new Card(Card.FOUR, Card.CLUBS),
		new Card(Card.EIGHT, Card.CLUBS),
		new Card(Card.NINE, Card.CLUBS),
		new Card(Card.JACK, Card.CLUBS)
	);
	private static final Hand KING_HIGH_FLUSH = new Hand(
		new Card(Card.FOUR, Card.SPADES),
		new Card(Card.TWO, Card.SPADES),
		new Card(Card.KING, Card.SPADES),
		new Card(Card.EIGHT, Card.SPADES),
		new Card(Card.TEN, Card.SPADES)
	);

	@Test
	public void queenHighBeatsSevenHigh() {
		Assert.assertEquals(-1, SEVEN_HIGH.compareTo(QUEEN_HIGH));
		Assert.assertEquals(+1, QUEEN_HIGH.compareTo(SEVEN_HIGH));
	}

	@Test
	public void pairBeatsHighCard() {
		Assert.assertEquals(-1, QUEEN_HIGH.compareTo(PAIR_OF_TWO));
		Assert.assertEquals(+1, PAIR_OF_TWO.compareTo(QUEEN_HIGH));
	}

	@Test
	public void highPairBeatsLowPair() {
		Assert.assertEquals(-1, PAIR_OF_TWO.compareTo(PAIR_OF_NINE));
		Assert.assertEquals(+1, PAIR_OF_NINE.compareTo(PAIR_OF_TWO));
	}

	@Test
	public void tripsBeatsPair() {
		Assert.assertEquals(-1, PAIR_OF_NINE.compareTo(TRIPS_OF_EIGHT));
		Assert.assertEquals(+1, TRIPS_OF_EIGHT.compareTo(PAIR_OF_NINE));
	}

	@Test
	public void highTripBeatsLowTrip() {
		Assert.assertEquals(-1, TRIPS_OF_EIGHT.compareTo(TRIPS_OF_JACK));
		Assert.assertEquals(+1, TRIPS_OF_JACK.compareTo(TRIPS_OF_EIGHT));
	}

	@Test
	public void flushBeatsTrips() {
		Assert.assertEquals(-1, TRIPS_OF_JACK.compareTo(JACK_HIGH_FLUSH));
		Assert.assertEquals(+1, JACK_HIGH_FLUSH.compareTo(TRIPS_OF_JACK));
	}

	@Test
	public void highFlushBeatsLowFlush() {
		Assert.assertEquals(-1, JACK_HIGH_FLUSH.compareTo(KING_HIGH_FLUSH));
		Assert.assertEquals(+1, KING_HIGH_FLUSH.compareTo(JACK_HIGH_FLUSH));
	}
}
