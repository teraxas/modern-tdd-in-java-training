package se.seb.training.tdd.ex1.coinchanger;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * Created by S86571 on 2017.04.26.
 */
public class CoinChangerTests {

	@Test
	public void correctChangeWhenUsingOneCoinType() {
		double[] coinTypes = { 1.0 };
		CoinChanger sut = new CoinChanger(coinTypes);

		Map<Double, Integer> change = sut.makeChange(14);

		Assert.assertEquals(14, (int) change.get(1.0));
	}

	@Test
	public void correctChangeWhenUsingMultipleCoinTypes() {
		double[] coinTypes = { 2.0, 1.0 };
		CoinChanger sut = new CoinChanger(coinTypes);

		Map<Double, Integer> change = sut.makeChange(15);

		Assert.assertEquals(7, (int) change.get(2.0));
		Assert.assertEquals(1, (int) change.get(1.0));
	}

	@Test
	public void correctChangeDifferentOrder() {
		double[] coinTypes = { 1.0, 2.0 };
		CoinChanger sut = new CoinChanger(coinTypes);

		Map<Double, Integer> change = sut.makeChange(15);

		Assert.assertEquals(7, (int) change.get(2.0));
		Assert.assertEquals(1, (int) change.get(1.0));
	}

	@Test(expected = CoinChanger.CoinChangeException.class)
	public void changeFailure() {
		double[] coinTypes = { 2.0};
		CoinChanger sut = new CoinChanger(coinTypes);

		Map<Double, Integer> change = sut.makeChange(15);
	}

	@Test(expected = CoinChanger.CoinChangeException.class)
	public void negativeMoney() {
		double[] coinTypes = { 2.0};
		CoinChanger sut = new CoinChanger(coinTypes);

		Map<Double, Integer> change = sut.makeChange(-14);
	}
}
