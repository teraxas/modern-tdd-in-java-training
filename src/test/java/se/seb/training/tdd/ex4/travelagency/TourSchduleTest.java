package se.seb.training.tdd.ex4.travelagency;

import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by S86571 on 2017.04.26.
 */
public class TourSchduleTest {
	private static final int MAX_TOURS_IN_DAY = 3;

	private TourSchedule sut;

	@Before
	public void before() {
		this.sut = new TourSchedule(MAX_TOURS_IN_DAY);
	}

	@Test
	public void canCreateNewTour() {
		String name = "TestTour";
		LocalDate date = new LocalDate(2017, 04, 27);
		int seats = 20;

		sut.createTour(name, date, seats);
		List<Tour> tours = sut.getToursFor(date);

		Assert.assertEquals(tours.size(), 1);
		Assert.assertEquals(tours.get(0).getName(), name);
		Assert.assertEquals(tours.get(0).getDate(), date);
		Assert.assertEquals(tours.get(0).getSeats(), seats);
	}

	@Test
	public void canCreateTwoNewTours() {
		LocalDate date = new LocalDate(2017, 04, 27);
		String name0 = "TestTour";
		String name1 = "TestTour1";
		int seats0 = 20;
		int seats1 = 25;

		sut.createTour(name0, date, seats0);
		sut.createTour(name1, date, seats1);
		List<Tour> tours = sut.getToursFor(date);

		Assert.assertEquals(tours.size(), 2);
		Assert.assertEquals(tours.get(0).getName(), name0);
		Assert.assertEquals(tours.get(0).getDate(), date);
		Assert.assertEquals(tours.get(0).getSeats(), seats0);
		Assert.assertEquals(tours.get(1).getName(), name1);
		Assert.assertEquals(tours.get(1).getDate(), date);
		Assert.assertEquals(tours.get(1).getSeats(), seats1);
	}


	@Test
	public void canGetToursOnGivenDateOnly() {
		LocalDate date = new LocalDate(2017, 04, 27);
		LocalDate date1 = new LocalDate(2017, 04, 28);
		String name0 = "TestTour";
		String name1 = "TestTour1";
		int seats0 = 20;
		int seats1 = 25;

		sut.createTour(name0, date, seats0);
		sut.createTour(name1, date1, seats1);
		sut.createTour(name1, date1, seats1);
		sut.createTour(name1, date1, seats1);
		List<Tour> tours = sut.getToursFor(date);
		List<Tour> tours1 = sut.getToursFor(date1);

		Assert.assertEquals(tours.size(), 1);
		Assert.assertEquals(tours1.size(), 3);
	}

	@Test(expected = TourSchedule.FullyBookedException.class)
	public void overbookedDay() {
		String name = "TestTour";
		LocalDate date = new LocalDate(2017, 04, 27);
		int seats = 20;

		sut.createTour(name, date, seats);
		sut.createTour(name, date, seats);
		sut.createTour(name, date, seats);
		sut.createTour(name, date, seats);
	}

	@Test
	public void overbookedDayWithNextAvailable() {
		String name = "TestTour";
		LocalDate date = new LocalDate(2017, 04, 27);
		int seats = 20;

		sut.createTour(name, date, seats);
		sut.createTour(name, date, seats);
		sut.createTour(name, date, seats);

		try {
			sut.createTour(name, date, seats);
		} catch (TourSchedule.FullyBookedException e) {
			sut.createTour(name, e.getAvailableDate(), seats);
			Assert.assertEquals(sut.getToursFor(e.getAvailableDate()).size(), 1);
		}
	}

}
