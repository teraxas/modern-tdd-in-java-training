package se.seb.training.tdd.ex4.travelagency;

import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by S86571 on 2017.04.27.
 */
public class BookingSystemTest {

	public static final String TEST_TOUR_NAME = "TestTour";
	public static final LocalDate TEST_TOUR_DATE = new LocalDate(2017, 4, 27);

	private TourScheduleStub tourSchedule;
	private BookingSystem sut;

	@Before
	public void before() {
		this.tourSchedule = new TourScheduleStub();
		this.sut = new BookingSystem(this.tourSchedule);
	}

	@Test
	public void canCreateBooking() {
		Tour tour = new Tour(TEST_TOUR_NAME, TEST_TOUR_DATE, 5);
		tourSchedule.tours = Arrays.asList(tour);
		tourSchedule.tourByName = tour;
		Passenger passenger = new Passenger("John", "Doe");

		sut.createBooking(TEST_TOUR_NAME, TEST_TOUR_DATE, passenger);
		List<Booking> bookings = sut.getBookingsFor(passenger);

		Assert.assertEquals(tourSchedule.getToursForCalls.size(), 1);
		Assert.assertEquals(tourSchedule.getToursForCalls.get(0), TEST_TOUR_DATE);
		Assert.assertEquals(bookings.size(), 1);
		Assert.assertEquals(bookings.get(0).getPassenger(), passenger);
	}


	@Test(expected = BookingSystem.TourDoesNotExistException.class)
	public void cantCreateBookingForNonExistingTour() {
		Passenger passenger = new Passenger("John", "Doe");
		sut.createBooking(TEST_TOUR_NAME, TEST_TOUR_DATE, passenger);
	}

	@Test(expected = BookingSystem.FullyBookedException.class)
	public void cantOverbook() {
		Tour tour = new Tour(TEST_TOUR_NAME, TEST_TOUR_DATE, 3);
		tourSchedule.tours = Arrays.asList(tour);
		tourSchedule.tourByName = tour;
		List<Passenger> passengers = Arrays.asList(new Passenger("John0", "Doe"),
				new Passenger("John1", "Doe"),
				new Passenger("John2", "Doe"),
				new Passenger("John3", "Doe"));

		passengers.stream()
				.forEach(p -> sut.createBooking(TEST_TOUR_NAME, TEST_TOUR_DATE, p));

	}
}
