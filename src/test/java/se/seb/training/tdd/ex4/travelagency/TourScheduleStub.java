package se.seb.training.tdd.ex4.travelagency;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by S86571 on 2017.04.27.
 */
public class TourScheduleStub implements ITourSchedule {

	public List<Tour> tours;
	public Tour tourByName;
	public LocalDate unbookedDate;
	public List<LocalDate> getToursForCalls = new ArrayList<>();

	@Override
	public void createTour(String name, LocalDate date, int seats) {
		throw new RuntimeException("Not implemented");
	}

	@Override
	public List<Tour> getToursFor(LocalDate date) {
		getToursForCalls.add(date);
		return tours;
	}

	@Override
	public LocalDate getNextUnbookedDate(LocalDate date) {
		return unbookedDate;
	}

	@Override
	public Tour getTourByName(LocalDate date, String tourName) {
		List<Tour> tours = Optional.ofNullable(getToursFor(date)).orElse(new ArrayList<>());
		return tours.isEmpty() ? null : tours.get(0);
	}
}
