package se.edument.domain.framework;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Before;

public class BDDTest<TCommandHandler, TAggregate extends BaseAggregate> {
	protected TCommandHandler sut;
	
	@Before
	public void before() {
		sut = createCommandHandler();
	}
	
	protected <TCommand> void test(List<?> given, Applier<TCommand> when, Checker then) {
		then.check(when.apply(applyEvents(newAggregate(), given)));
	}
	
	protected List<?> given(Object... events) {
		return Arrays.asList(events);
	}
	
	protected <TCommand> Applier<TCommand> when(TCommand command) {
		return new Applier<TCommand>(command);
	}
	
	protected Checker then(Object... events) {
		List<?> expected = Arrays.asList(events);
		return new ExpectedEventsChecker(expected);
	}
	
	protected Checker thenFailWith(Class<? extends Exception> exception) {
		return new ExpectedExceptionChecker(exception);
	}
	
	private class Applier<TCommand> {
		private TCommand command;
		
		public Applier(TCommand command) {
			this.command = command;
		}
		
		public EventsOrException apply(final TAggregate agg) {
			EventsOrException eoe;
			ILoadAggregate<TAggregate> al = new ILoadAggregate<TAggregate>() {
				public TAggregate load(UUID id) {
					return agg;
				}
			};
			try {
				eoe = EventsOrException.events(dispatchCommand(al, command));
			}
			catch (RuntimeException ex) {
				eoe = EventsOrException.exception(ex);
			}
			return eoe;
		}
	}
	
	static private abstract class Checker {
		public abstract void check(EventsOrException actual);
	}
	
	static private class ExpectedEventsChecker extends Checker {
		private List<?> expected;
		
		public ExpectedEventsChecker(List<?> events) {
			expected = events;
		}
		
		String name(Object o) {
			return o.getClass().getSimpleName();
		}
		
		String nameAll(List<?> list) {
			StringBuilder sb = new StringBuilder();
			sb.append('[');
			for (Object o : list)
				sb.append(name(o));
			sb.append(']');
			return sb.toString();
		}

		public void check(EventsOrException actual) {
			if (actual.isEvents) {
				for (Object exp : expected) {
					if (actual.events.size() == 0)
						Assert.fail("Not enough events in the results; expected a " + name(exp));
						
					Object found = actual.events.get(0);
					
					if (!name(found).equals(name(exp)))
						Assert.fail("Incorrect event in results; expected a " + name(exp) + " but got a " + name(found));
						
					Assert.assertEquals(serialize(exp), serialize(found));
					
					actual.events.remove(0);
				}
				
				if (actual.events.size() > 0)
					Assert.fail("Got too many events back; did not expect " + nameAll(actual.events));
			}
			else {
				throw new IllegalStateException("Unexpected exception in test: ", actual.exception);
			}
		}
	}
	
	static private class ExpectedExceptionChecker extends Checker {
		private Class<? extends Exception> expected;
		
		public ExpectedExceptionChecker(Class<? extends Exception> exceptionClass) {
			expected = exceptionClass;
		}

		public void check(EventsOrException actual) {
			if (actual.isEvents){
				String maybe_s = actual.events.size() == 1 ? "s" : "";
				Assert.fail("Expected exception, but got event" + maybe_s);
			}
			else {
				if (expected.getName().equals(actual.exception.getClass().getName()))
					Assert.fail("Got exception but it was the wrong type");
				else
					Assert.assertTrue("Got correct exception type", true);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private TCommandHandler createCommandHandler() {
		try {
			Type type = getClass().getGenericSuperclass();
			ParameterizedType paramType = (ParameterizedType) type;
			Class<TCommandHandler> clazz = (Class<TCommandHandler>) paramType.getActualTypeArguments()[0];
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException ex) {
			throw new IllegalArgumentException("Could not instantiate command handler", ex);
		}
	}
	
	@SuppressWarnings("unchecked")
	private TAggregate newAggregate() {
		try {
			Type type = getClass().getGenericSuperclass();
			ParameterizedType paramType = (ParameterizedType) type;
			Class<TAggregate> clazz = (Class<TAggregate>) paramType.getActualTypeArguments()[1];
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException ex) {
			throw new IllegalArgumentException("Could not instantiate aggregate", ex);
		}
	}
	
	private TAggregate applyEvents(TAggregate agg, List<?> events) {
		agg.applyEvents(events);
		return agg;
	}

	private <TCommand> List<?> dispatchCommand(ILoadAggregate<TAggregate> al, TCommand c) {
		try {
			Class<?> cl = sut.getClass();
			if (cl == null)
				throw new IllegalStateException("The SUT is null -- did you run the before() method?");
			Method m = cl.getDeclaredMethod("handleCommand", ILoadAggregate.class, c.getClass());
			List<?> events = (List<?>)m.invoke(sut, al, c);
			if (events == null)
				events = Collections.EMPTY_LIST;
			return events;
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException
				| SecurityException ex) {
			throw new IllegalArgumentException("The command handler cannot handle " + c.getClass().getSimpleName(), ex);
		}
	}
	
	static private class EventsOrException {
		private boolean isEvents;
		private List<?> events;
		private Exception exception;
		
		private EventsOrException() {
		}
		
		public static EventsOrException events(List<?> events) {
			EventsOrException eoe = new EventsOrException();
			eoe.isEvents = true;
			eoe.events = events;
			return eoe;
		}
		
		public static EventsOrException exception(Exception exception) {
			EventsOrException eoe = new EventsOrException();
			eoe.isEvents = false;
			eoe.exception = exception;
			return eoe;
		}
	}
	
	private static String serialize(Object event) {
		StringBuilder sb = new StringBuilder();
		for (Field field : event.getClass().getDeclaredFields()) {
			String type = field.getType().getName();
			String value = "";
			try {
				if (field.get(event) == null) {
					value = "null";
				}
				else if (type.equals("java.util.UUID")) {
					value = ((UUID)field.get(event)).toString();
				}
				else if (type.equals("int")) {
					value = Integer.toString(field.getInt(event));
				}
				else if (type.equals("java.lang.String")) {
					value = (String)field.get(event);
				}
				else if (type.equals("org.joda.time.DateTime")) {
					value = ((DateTime)field.get(event)).toString();
				}
				else if (type.equals("org.joda.time.LocalDate")) {
					value = ((LocalDate)field.get(event)).toString();
				}
				else {
					throw new IllegalArgumentException("Unknown field type: " + type);
				}
			} catch (IllegalArgumentException | IllegalAccessException ex) {
				throw new IllegalArgumentException("Couldn't serialize event", ex);
			}
			sb.append(field.getName()).append(':').append(value).append('\n');
		}
		return sb.toString();
	}
}
