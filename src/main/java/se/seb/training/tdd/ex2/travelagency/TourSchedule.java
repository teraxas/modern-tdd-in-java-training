package se.seb.training.tdd.ex2.travelagency;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by S86571 on 2017.04.26.
 */
public class TourSchedule {

	private int maxToursInDay;
	private List<Tour> tours = new ArrayList<>();

	public TourSchedule(int maxToursInDay) {
		this.maxToursInDay = maxToursInDay;
	}

	public void createTour(String name, LocalDate date, int seats) {
		if (name == null || date == null || seats <= 0) {
			throw new IllegalArgumentException();
		}
		if (isOverbooked(date)) {
			throw getExceptionWithNextValidDate(date);
		}

		Tour tour = new Tour(name, date, seats);
		this.tours.add(tour);
	}

	public List<Tour> getToursFor(LocalDate date) {
		return tours.stream()
				.filter(t -> t.getDate().equals(date))
				.collect(Collectors.toList());
	}

	public LocalDate getNextUnbookedDate(LocalDate date) {
		LocalDate nextdate = date;
		do {
			nextdate = nextdate.plusDays(1);
		} while (isOverbooked(nextdate));
		return nextdate;
	}

	private boolean isOverbooked(LocalDate date) {
		return tours.stream()
				.filter(t -> t.getDate().equals(date))
				.count() >= maxToursInDay;
	}

	private FullyBookedException getExceptionWithNextValidDate(LocalDate date) {
		LocalDate nextdate = getNextUnbookedDate(date);

		return new FullyBookedException(nextdate);
	}

	public static class FullyBookedException extends RuntimeException {
		private LocalDate availableDate;

		public FullyBookedException(LocalDate availableDate) {
			super("Next available date: " + availableDate.toString());
			this.availableDate = availableDate;
		}

		public LocalDate getAvailableDate() {
			return availableDate;
		}
	}
}
