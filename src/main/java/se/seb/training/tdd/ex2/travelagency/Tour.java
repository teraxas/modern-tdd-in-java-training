package se.seb.training.tdd.ex2.travelagency;

import org.joda.time.LocalDate;

/**
 * Created by S86571 on 2017.04.26.
 */
public class Tour {
	private final String name;
	private final LocalDate date;
	private final int seats;

	public Tour(String name, LocalDate date, int seats) {
		this.name = name;
		this.date = date;
		this.seats = seats;
	}

	public String getName() {
		return name;
	}

	public LocalDate getDate() {
		return date;
	}

	public int getSeats() {
		return seats;
	}
}
