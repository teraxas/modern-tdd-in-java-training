package se.seb.training.tdd.ex4.travelagency;

/**
 * Created by S86571 on 2017.04.27.
 */
public class Passenger {

	private final String firstName;
	private final String lastName;

	public Passenger(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Passenger passenger = (Passenger) o;

		if (firstName != null ? !firstName.equals(passenger.firstName) : passenger.firstName != null) return false;
		return lastName != null ? lastName.equals(passenger.lastName) : passenger.lastName == null;

	}

	@Override
	public int hashCode() {
		int result = firstName != null ? firstName.hashCode() : 0;
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		return result;
	}
}
