package se.seb.training.tdd.ex4.travelagency;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by S86571 on 2017.04.27.
 */
public class BookingSystem {
	private ITourSchedule tourSchedule;

	private List<Booking> bookings = new ArrayList<>();

	public BookingSystem(ITourSchedule tourSchedule) {
		this.tourSchedule = tourSchedule;
	}

	public void createBooking(String tourName, LocalDate localDate, Passenger passenger) {
		Tour tour = this.tourSchedule.getTourByName(localDate, tourName);
		if (tour == null) {
			throw new TourDoesNotExistException(tourName);
		}
		if (isFullyBooked(tour)) {
			throw new FullyBookedException(tourName);
		}

		Booking booking = new Booking(passenger, tour);
		bookings.add(booking);
	}

	public List<Booking> getBookingsFor(Passenger passenger) {
		return bookings.stream()
				.filter(b -> b.getPassenger().equals(passenger))
				.collect(Collectors.toList());
	}

	private boolean isFullyBooked(Tour tour) {
		return bookings.stream()
				.filter(b -> b.getTour().equals(tour))
				.count() >= tour.getSeats();
	}

	public static class TourDoesNotExistException extends RuntimeException {
		public TourDoesNotExistException(String tourName) {
			super("Tour doesn't exist: " + tourName);
		}
	}

	public class FullyBookedException extends RuntimeException {
		public FullyBookedException(String tourName) {
			super("Tour overbooked: " + tourName);
		}
	}
}
