package se.seb.training.tdd.ex4.travelagency;

/**
 * Created by S86571 on 2017.04.27.
 */
public class Booking {

	private Passenger passenger;
	private Tour tour;

	public Booking(Passenger passenger, Tour tour) {
		this.passenger = passenger;
		this.tour = tour;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public Tour getTour() {
		return tour;
	}
}
