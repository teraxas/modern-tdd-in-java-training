package se.seb.training.tdd.ex7.travelagency;

import org.joda.time.LocalDate;

import java.util.UUID;

/**
 * Created by S86571 on 2017.04.27.
 */
public class TourScheduled {
	public final UUID id;
	public final String name;
	public final LocalDate date;
	public final int seats;

	public TourScheduled(String name, LocalDate date, int seats, UUID id) {
		this.id = id;
		this.name = name;
		this.date = date;
		this.seats = seats;
	}

	public TourScheduled(ScheduleTour command) {
		this.id = command.getId();
		this.name = command.getName();
		this.date = command.getDate();
		this.seats = command.getSeats();
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public LocalDate getDate() {
		return date;
	}

	public int getSeats() {
		return seats;
	}
}
