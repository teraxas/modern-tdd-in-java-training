package se.seb.training.tdd.ex7.travelagency;

import org.joda.time.LocalDate;
import se.edument.domain.framework.BaseAggregate;

/**
 * Created by S86571 on 2017.04.27.
 */
public class TourScheduledAggregate extends BaseAggregate {

	int toursCount = 0;

	public void applyEvent(ScheduleTour command) {
		// Schedule
		this.toursCount++;
	}

	public boolean isTooManyTours(LocalDate date) {
		return toursCount == 3;
	}

}
