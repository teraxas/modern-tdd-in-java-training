package se.seb.training.tdd.ex7.travelagency;

import se.edument.domain.framework.ILoadAggregate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by S86571 on 2017.04.27.
 */
public class TourScheduleCommandHandler {

	public List<?> handleCommand(ILoadAggregate<TourScheduledAggregate> al, ScheduleTour c) {
		TourScheduledAggregate aggregate = al.load(c.getId());
		if (aggregate.isTooManyTours(c.getDate())) {
			throw new TooManyToursException();
		}

		ArrayList<Object> objects = new ArrayList<>();
		objects.add(new TourScheduled(c));
		return objects;
	}
}
