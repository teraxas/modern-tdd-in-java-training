package se.seb.training.tdd.ex5.travelagency;

/**
 * Created by S86571 on 2017.04.27.
 */
public class Passenger {

	private final String firstName;
	private final String lastName;
	private final String email;

	public Passenger(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Passenger passenger = (Passenger) o;

		if (!firstName.equals(passenger.firstName)) return false;
		if (!lastName.equals(passenger.lastName)) return false;
		return email.equals(passenger.email);

	}

	@Override
	public int hashCode() {
		int result = firstName.hashCode();
		result = 31 * result + lastName.hashCode();
		result = 31 * result + email.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "Passenger{" +
				"firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", email='" + email + '\'' +
				'}';
	}
}
