package se.seb.training.tdd.ex5.travelagency;

/**
 * Created by S86571 on 2017.04.27.
 */
public class Booking {

	private Passenger passenger;
	private Tour tour;

	public Booking(Passenger passenger, Tour tour) {
		this.passenger = passenger;
		this.tour = tour;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public Tour getTour() {
		return tour;
	}

	@Override
	public String toString() {
		return "Booking{" +
				"passenger=" + passenger.toString() +
				", tour=" + tour.toString() +
				'}';
	}
}
