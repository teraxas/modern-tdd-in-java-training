package se.seb.training.tdd.ex5.travelagency;

/**
 * Created by S86571 on 2017.04.27.
 */
public interface IMailSender {
	void sendMail(String email, String message);
}
