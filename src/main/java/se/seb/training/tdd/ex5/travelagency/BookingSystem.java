package se.seb.training.tdd.ex5.travelagency;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by S86571 on 2017.04.27.
 */
public class BookingSystem {
	public static final String BOOKING_CREATED_EMAIL = "Booking created: %s";
	private ITourSchedule tourSchedule;
	private IMailSender mailSender;

	private List<Booking> bookings = new ArrayList<>();

	public BookingSystem(ITourSchedule tourSchedule, IMailSender mailSender) {
		this.tourSchedule = tourSchedule;
		this.mailSender = mailSender;
	}

	public void createBooking(String tourName, LocalDate localDate, Passenger passenger) {
		Tour tour = this.tourSchedule.getTourByName(localDate, tourName);
		if (tour == null) {
			throw new TourDoesNotExistException(tourName);
		}
		if (isFullyBooked(tour)) {
			throw new FullyBookedException(tourName);
		}

		Booking booking = new Booking(passenger, tour);
		bookings.add(booking);

		notifyPassenger(passenger, booking);
	}

	public List<Booking> getBookingsFor(Passenger passenger) {
		return bookings.stream()
				.filter(b -> b.getPassenger().equals(passenger))
				.collect(Collectors.toList());
	}

	private void notifyPassenger(Passenger passenger, Booking booking) {
		mailSender.sendMail(passenger.getEmail(), String.format(BOOKING_CREATED_EMAIL, booking.toString()));
	}

	private boolean isFullyBooked(Tour tour) {
		return bookings.stream()
				.filter(b -> b.getTour().equals(tour))
				.count() >= tour.getSeats();
	}

	public static class TourDoesNotExistException extends RuntimeException {
		public TourDoesNotExistException(String tourName) {
			super("Tour doesn't exist: " + tourName);
		}
	}

	public class FullyBookedException extends RuntimeException {
		public FullyBookedException(String tourName) {
			super("Tour overbooked: " + tourName);
		}
	}
}
