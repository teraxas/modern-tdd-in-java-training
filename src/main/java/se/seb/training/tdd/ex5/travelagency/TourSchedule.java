package se.seb.training.tdd.ex5.travelagency;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by S86571 on 2017.04.26.
 */
public class TourSchedule implements ITourSchedule {

	private int maxToursInDay;
	private List<Tour> tours = new ArrayList<>();

	public TourSchedule(int maxToursInDay) {
		this.maxToursInDay = maxToursInDay;
	}

	@Override
	public void createTour(String name, LocalDate date, int seats) {
		if (name == null || date == null || seats <= 0) {
			throw new IllegalArgumentException();
		}
		if (isOverbooked(date)) {
			throw getExceptionWithNextValidDate(date);
		}

		Tour tour = new Tour(name, date, seats);
		this.tours.add(tour);
	}

	@Override
	public List<Tour> getToursFor(LocalDate date) {
		return tours.stream()
				.filter(t -> t.getDate().equals(date))
				.collect(Collectors.toList());
	}

	@Override
	public LocalDate getNextUnbookedDate(LocalDate date) {
		LocalDate nextdate = date;
		do {
			nextdate = nextdate.plusDays(1);
		} while (isOverbooked(nextdate));
		return nextdate;
	}

	@Override
	public Tour getTourByName(LocalDate date, String tourName) {
		return getToursFor(date).stream()
				.filter(t -> t.getName().equals(tourName))
				.findAny()
				.orElse(null);
	}

	private boolean isOverbooked(LocalDate date) {
		return tours.stream()
				.filter(t -> t.getDate().equals(date))
				.count() >= maxToursInDay;
	}

	private FullyBookedException getExceptionWithNextValidDate(LocalDate date) {
		LocalDate nextdate = getNextUnbookedDate(date);

		return new FullyBookedException(nextdate);
	}

}
