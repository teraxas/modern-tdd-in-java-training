package se.seb.training.tdd.ex5.travelagency;

import org.joda.time.LocalDate;

/**
 * Created by S86571 on 2017.04.26.
 */
public class Tour {
	private final String name;
	private final LocalDate date;
	private final int seats;

	public Tour(String name, LocalDate date, int seats) {
		this.name = name;
		this.date = date;
		this.seats = seats;
	}

	public String getName() {
		return name;
	}

	public LocalDate getDate() {
		return date;
	}

	public int getSeats() {
		return seats;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Tour tour = (Tour) o;

		if (name != null ? !name.equals(tour.name) : tour.name != null) return false;
		return date != null ? date.equals(tour.date) : tour.date == null;

	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (date != null ? date.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Tour{" +
				"name='" + name + '\'' +
				", date=" + date +
				", seats=" + seats +
				'}';
	}
}
