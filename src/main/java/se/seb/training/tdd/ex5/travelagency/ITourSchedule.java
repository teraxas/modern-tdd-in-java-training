package se.seb.training.tdd.ex5.travelagency;

import org.joda.time.LocalDate;

import java.util.List;

/**
 * Created by S86571 on 2017.04.27.
 */
public interface ITourSchedule {
	void createTour(String name, LocalDate date, int seats);

	List<Tour> getToursFor(LocalDate date);

	LocalDate getNextUnbookedDate(LocalDate date);

	Tour getTourByName(LocalDate date, String tourName);

	class FullyBookedException extends RuntimeException {
		private LocalDate availableDate;

		public FullyBookedException(LocalDate availableDate) {
			super("Next available date: " + availableDate.toString());
			this.availableDate = availableDate;
		}

		public LocalDate getAvailableDate() {
			return availableDate;
		}
	}
}
