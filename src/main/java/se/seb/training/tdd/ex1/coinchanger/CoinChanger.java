package se.seb.training.tdd.ex1.coinchanger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by S86571 on 2017.04.26.
 */
public class CoinChanger {

	private double[] coinTypes;

	public CoinChanger(double[] coinTypes) {
		double[] copy = Arrays.copyOf(coinTypes, coinTypes.length);
		Arrays.sort(copy);
		reverse(copy);
		this.coinTypes = copy;
	}

	public Map<Double, Integer> makeChange(int money) {
		if (money < 0) {
			throw new CoinChangeException("Negative money");
		}

		HashMap<Double, Integer> result = new HashMap<>();
		for (double coin : coinTypes) {
			int coinsRequired = (int) (money / coin);
			money -= coin * coinsRequired;
			result.put(coin, coinsRequired);
		}

		if (money > 0) {
			throw new CoinChangeException("Cannot Change");
		}
		return result;
	}

	public static class CoinChangeException extends RuntimeException {
		public CoinChangeException(String s) {
			super(s);
		}
	}

	private void reverse(double[] array) {
		for (int i = 0; i < array.length / 2; i++) {
			double temp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = temp;
		}
	}
}
