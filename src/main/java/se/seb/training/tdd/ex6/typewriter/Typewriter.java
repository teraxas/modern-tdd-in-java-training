package se.seb.training.tdd.ex6.typewriter;

import java.util.function.Consumer;

/**
 * Created by S86571 on 2017.04.27.
 */
public class Typewriter {

	private Consumer<String> logger;

	public void setLogger(Consumer<String> logger) {
		this.logger = logger;
	}

	public void sendInput(String s) {
		if (logger != null) {
			logger.accept(s);
		}
	}
}
