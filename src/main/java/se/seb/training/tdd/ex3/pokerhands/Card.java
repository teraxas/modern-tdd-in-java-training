package se.seb.training.tdd.ex3.pokerhands;

public class Card {
	// Ranks
	public static final String ACE = "ACE";
	public static final String TWO = "TWO";
	public static final String THREE = "THREE";
	public static final String FOUR = "FOUR";
	public static final String FIVE = "FIVE";
	public static final String SIX = "SIX";
	public static final String SEVEN = "SEVEN";
	public static final String EIGHT = "EIGHT";
	public static final String NINE = "NINE";
	public static final String TEN = "TEN";
	public static final String JACK = "JACK";
	public static final String QUEEN = "QUEEN";
	public static final String KING = "KING";
	public static final String[] RANKS =
		{ TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE };
	
	// Suits
	public static final String CLUBS = "CLUBS";
	public static final String DIAMONDS = "DIAMONDS";
	public static final String HEARTS = "HEARTS";
	public static final String SPADES = "SPADES";
	
	private String rank;
	private String suit;

	public Card(String rank, String suit) {
		this.rank = rank;
		this.suit = suit;
	}
	
	public Integer getRankIndex() {
		for (int i = 0; i < RANKS.length; i++) {
			if (RANKS[i].equals(this.rank)) {
				return i;
			}
		}
		throw new IllegalArgumentException();
	}

	public String getRank() {
		return rank;
	}

	public String getSuit() {
		return suit;
	}
}
