package se.seb.training.tdd.ex3.pokerhands;

import java.util.Comparator;

/**
 * Created by S86571 on 2017.04.26.
 */
public enum HandType {
	HIGH_CARD((h1, h2) -> h1.compareHighestHand(h2)),
	PAIR((h1, h2) -> h1.compareHandsOfSameType(h2, h1.pairRank(), h2.pairRank())),
	TRIPS((h1, h2) -> h1.compareHandsOfSameType(h2, h1.tripsRank(), h2.tripsRank())),
	FLUSH((h1, h2) -> h1.compareHighestHand(h2))
	;

	private Comparator<Hand> sameHandTypeComparator;

	HandType(Comparator<Hand> sameHandTypeComparator) {
		this.sameHandTypeComparator = sameHandTypeComparator;
	}

	public int compareHandsOfSameType(Hand o1, Hand o2) {
		return sameHandTypeComparator.compare(o1,  o2);
	}

}
