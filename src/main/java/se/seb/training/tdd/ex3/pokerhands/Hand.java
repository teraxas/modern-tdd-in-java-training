package se.seb.training.tdd.ex3.pokerhands;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Hand implements Comparable<Hand> {

	private List<Card> cards;
	private static final Comparator<Card> BY_RANK = (a, b) -> a.getRankIndex().compareTo(b.getRankIndex());

	public Hand(Card... cards) {
		this.cards = Arrays.asList(cards);
		Collections.sort(this.cards, BY_RANK);
	}

	public HandType score() {
		if (hasFlush()) {
			return HandType.FLUSH;
		} else if (hasTrips()) {
			return HandType.TRIPS;
		} else if (hasPair()) {
			return HandType.PAIR;
		} else {
			return HandType.HIGH_CARD;
		}
	}

	@Override
	public int compareTo(Hand otherHand) {
		HandType thisScore = score();
		HandType otherScore = otherHand.score();
		if (thisScore.equals(otherScore)) {
			return thisScore.compareHandsOfSameType(this, otherHand);
		} else {
			return thisScore.compareTo(otherScore);
		}
	}

	int compareHandsOfSameType(Hand otherHand, Integer thisHandRank, Integer otherHandRank) {
		if (thisHandRank.compareTo(otherHandRank) != 0) {
			return thisHandRank.compareTo(otherHandRank);
		} else {
			return compareHighestHand(otherHand);
		}
	}

	int compareHighestHand(Hand otherHand) {
		Integer thisHandHighest = cards.get(4).getRankIndex();
		Integer otherHandHighest = otherHand.cards.get(4).getRankIndex();
		return thisHandHighest.compareTo(otherHandHighest);
	}

	Integer pairRank() {
		for (int i = 0; i < 4; i++) {
			if (sameRank(i, i + 1)) {
				return i;
			}
		}
		return -1;
	}

	Integer tripsRank() {
		for (int i = 0; i < 3; i++) {
			if (sameRank(i, i + 2)) {
				return i;
			}
		}
		return -1;
	}

	private boolean sameSuit(int... indexes) {
		return Arrays.stream(indexes)
				.mapToObj(idx -> cards.get(idx).getSuit())
				.distinct()
				.count() == 1;
	}

	private boolean sameRank(int... indexes) {
		return Arrays.stream(indexes)
				.mapToObj(idx -> cards.get(idx).getRank())
				.distinct()
				.count() == 1;
	}

	private boolean hasFlush() {
		return sameSuit(0, 1, 2, 3, 4);
	}

	private boolean hasTrips() {
		return sameRank(0, 2) || sameRank(1, 3) || sameRank(2, 4);
	}

	private boolean hasPair() {
		return sameRank(0, 1) || sameRank(1, 2) || sameRank(2, 3) || sameRank(3, 4);
	}

}
