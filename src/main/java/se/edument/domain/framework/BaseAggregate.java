package se.edument.domain.framework;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;

public class BaseAggregate {
	private int eventsLoaded = 0;
	private UUID id;
	
	public int eventsLoaded() {
		return eventsLoaded;
	}
	
	public UUID id() {
		return id;
	}
	
	protected void setId(UUID newId) {
		id = newId;
	}
	
	public void applyEvents(List<?> events) {
		for (Object ev : events)
			applyOneEvent(ev);
	}
	
	public void applyOneEvent(Object ev) {
		try {
			Method applyMethod = this.getClass().getDeclaredMethod("applyEvent", ev.getClass());
			applyMethod.invoke(this, ev);
			eventsLoaded++;
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException ex) {
			throw new IllegalArgumentException("Cannot apply the event " + ev.getClass().getSimpleName(), ex);
		}
	}
}
