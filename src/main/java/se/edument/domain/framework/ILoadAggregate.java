package se.edument.domain.framework;

import java.util.UUID;

public abstract class ILoadAggregate<TAggregate> {
	public abstract TAggregate load(UUID id);
}
